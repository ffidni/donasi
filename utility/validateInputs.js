export default function validateInputs(elements) {
  elements.preventDefault();
  const form = elements.target;
  const inputs = Array.from(form.children[0].children);
  console.log(inputs);
  return inputs.every((input) => input.dataset.hasError === 'false');
}
