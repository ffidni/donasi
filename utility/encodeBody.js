export default function encodeBody(body) {
  let form = new FormData();
  for (let key in body) {
    form.append(key, body[key]);
  }
  return form;
}
