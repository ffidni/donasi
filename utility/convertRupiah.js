export default function convertRupiah(price) {
  price = parseInt(price);
  if (price < 0) return '0,00';
  const seperated = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return `Rp${seperated},00`;
}
