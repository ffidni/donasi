import axios from 'axios';

const axiosClient = axios.create({
  baseURL: 'https://api.darululumciracap.com',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

// axiosClient.interceptors.response.use(
//   function (response) {
//     return response;
//   },
//   function (err) {
//     let res = err.response;
//     console.log(res);
//     return Promise.reject(err);
//   }
// );

export default axiosClient;
