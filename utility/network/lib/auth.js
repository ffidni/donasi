import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode';
import axiosClient from '../apiClient';

export function register(data) {
  return axiosClient.post('/signup', JSON.stringify(data));
}

export function login(data) {
  return axiosClient.post('/signin', JSON.stringify(data));
}

export function getAccount(phoneNumber) {
  return axiosClient.get(`/koordinator/${phoneNumber}`);
}



export function checkTokenExpire(token) {
  if (token) {
    const tokenData = jwtDecode(token).exp;
    if (tokenData < Date.now() / 1000) {
      Cookies.remove('token');
      return true;
    }
    return false;
  }
  return false;
}
