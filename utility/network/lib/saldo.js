import axiosClient from '../apiClient';

export function getSaldo() {
  return axiosClient.get('/saldo');
}
