import axiosClient from '../apiClient';

export function getPengeluaran(id = false) {
  return axiosClient.get('/pengeluaran' + (id ? `/${id}` : ''));
}

export function addPengeluaran(data) {
  return axiosClient.post('/pengeluaran', JSON.stringify(data));
}

export function editPengeluaran(id, data) {
  return axiosClient.put(`/pengeluaran/${id}`, JSON.stringify(data));
}

export function deletePengeluaran(id) {
  return axiosClient.delete(`/pengeluaran/${id}`);
}
