import axiosClient from '../apiClient';

export function getDonatur(id = false) {
  return axiosClient.get('/donatur' + (id ? `/${id}` : ''));
}

export function addDonatur(data) {
  return axiosClient.post('/donatur', JSON.stringify(data));
}

export function editDonatur(id, data) {
  return axiosClient.put(`/donatur/${id}`, JSON.stringify(data));
}

export function deleteDonatur(id) {
  return axiosClient.delete(`/donatur/${id}`);
}
