import axiosClient from '../apiClient';

export function getAngkatan() {
  return axiosClient.get('/angkatan');
}

export function getAngkatanData(cb) {
  getAngkatan()
    .then((res) => {
      console.log(res);
      cb(res.data.angkatan);
    })
    .catch((err) => {
      console.log(err);
    });
}
