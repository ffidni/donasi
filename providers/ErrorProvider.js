import { createContext, useEffect, useState } from 'react';

import React from 'react';
import { Alert, Collapse } from '@mui/material';

export const ErrorContext = createContext();
export default function ErrorProvider({ children }) {
  const [errorData, setErrorData] = useState({
    open: false,
    text: '',
    variant: 'error',
  });

  function showAlert(text, variant = 'error') {
    hideAlert();
    setErrorData({ ...errorData, open: true, text: text, variant: variant });
  }

  function hideAlert() {
    setErrorData({ ...errorData, open: false });
  }

  const values = {
    showAlert,
    hideAlert,
  };

  useEffect(() => {
    if (open) {
      setTimeout(hideAlert, 5000);
    }
  }, [errorData.open]);

  return (
    <ErrorContext.Provider value={values}>
      <>
        {children}
        <Collapse
          in={errorData.open}
          sx={{ position: 'absolute', bottom: 0, width: '100%' }}
        >
          <Alert onClick={hideAlert} severity={errorData.variant}>
            {errorData.text}
          </Alert>
        </Collapse>
      </>
    </ErrorContext.Provider>
  );
}
