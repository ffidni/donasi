import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode';
import { useRouter } from 'next/router';
import { createContext, useEffect, useState } from 'react';
import axiosClient from '../utility/network/apiClient';
import { checkTokenExpire, getAccount } from '../utility/network/lib/auth';

export const AuthContext = createContext();

export function AuthProvider({ children }) {
  const router = useRouter();
  const [token, setToken] = useState(null);
  const [user, setUser] = useState(null);
  const [angkatan, setAngkatan] = useState([]);

  function getAccountInfo(token) {
    const phoneNumber = jwtDecode(token).id;
    getAccount(phoneNumber)
      .then((res) => {
        console.log(res);
        setUser(res.data.koordinator);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function validateUser() {
    const token = Cookies.get('token');
    console.log(token);
    if (token) {
      const isExpired = checkTokenExpire(token);
      if (isExpired) {
        Cookies.remove('token');
        setUser(null);
      } else {
        axiosClient.defaults.headers.common['Authorization'] = token;
        getAccountInfo(token);
      }
    } else {
      console.log('HI');
      setUser(null);
    }
  }

  function logout() {
    Cookies.remove('token');
    setUser(null);
    router.push('/login');
  }

  const values = {
    user,
    setUser,
    token,
    setToken,
    angkatan,
    setAngkatan,
    validateUser,
    logout,
  };

  useEffect(() => {
    validateUser();
  }, []);

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>;
}
