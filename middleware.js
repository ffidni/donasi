// middleware.ts
import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode';
import { NextResponse } from 'next/server';
import { checkTokenExpire, getAccount } from './utility/network/lib/auth';

const PUBLIC_FILE = /\.(.*)$/;

// This function can be marked `async` if using `await` inside
export function middleware(request) {
  const { pathname, origin } = request.nextUrl;
  console.log('Middleware', { pathname });

  if (
    pathname.startsWith('/_next') || // exclude Next.js internals
    pathname.startsWith('/api') || //  exclude all API routes
    pathname.startsWith('/static') || // exclude static files
    PUBLIC_FILE.test(pathname) // exclude all files in the public folder
  )
    return NextResponse.next();
  else {
    if (pathname === '/' || pathname === '/register')
      return NextResponse.redirect(origin + '/donasi', request.nextUrl);
    const token = request.cookies.get('token');
    if (token) {
      const isExpired = checkTokenExpire(token.value);
      if (isExpired) {
        Cookies.remove('token');
        NextResponse.redirect('/login', request.nextUrl);
      }
    }
    return NextResponse.next();
  }
}

// See "Matching Paths" below to learn more
