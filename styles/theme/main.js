import { createTheme } from '@mui/material';

const theme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: '#3E9362',
    },
    text: {
      primary: '#000',
    },
  },
  typography: {
    allVariants: {
      color: '#000',
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        contained: {
          borderRadius: 30,
        },
      },
    },
  },
});

export default theme;
