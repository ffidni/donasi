import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import { useRouter } from 'next/router';
import { ValidationGroup, Validate } from 'mui-validate';
import React, { useContext, useEffect, useRef, useState } from 'react';
import axiosClient from '../utility/network/apiClient';
import { getAngkatan, getAngkatanData } from '../utility/network/lib/angkatan';
import validateInputs from '../utility/validateInputs';
import { register } from '../utility/network/lib/auth';
import { AuthContext } from '../providers/AuthProvider';

function Register() {
  const router = useRouter();
  const { angkatan, setAngkatan, user } = useContext(AuthContext);
  const [confirm, setConfirm] = useState('');
  const [form, setForm] = useState({
    nama_koordinator: '',
    nomor_hp: '',
    id_angkatan: '1',
    password: '',
  });
  const passRef = useRef();
  const confirmRef = useRef();

  function onSubmit(elements) {
    const valid = validateInputs(elements);
    console.log(valid);

    if (valid) {
      register(form)
        .then((res) => {
          if (res.data.token) {
            window.localStorage.setItem('token', res.data.token);
            axiosClient.defaults.headers.common['Authorization'] =
              res.data.token;
            router.replace('/donasi');
          }
        })
        .then((err) => {
          console.log(err, 'GAGAL');
        });
    }
  }

  useEffect(() => {
    getAngkatanData(setAngkatan);
  }, []);

  return (
    <Stack mx={5}>
      <Typography variant="h5" textAlign="center">
        Akun
      </Typography>
      <form onSubmit={(e) => onSubmit(e)}>
        <Stack my={3} spacing={2.5}>
          <ValidationGroup>
            <Validate name="nama" required={[true, 'Harus diisi']}>
              <TextField
                variant="outlined"
                label="Nama Lengkap"
                value={form.nama_koordinator}
                onChange={(e) =>
                  setForm({ ...form, nama_koordinator: e.target.value })
                }
              />
            </Validate>
            <Validate name="nomor_hp" required={[true, 'Harus diisi']}>
              <TextField
                variant="outlined"
                label="Nomor HP"
                type="number"
                value={form.nomor_hp}
                onChange={(e) => setForm({ ...form, nomor_hp: e.target.value })}
              />
            </Validate>
            <Validate name="angkatan" required={[true, 'Harus diisi']}>
              <TextField
                variant="outlined"
                label="Angkatan"
                select
                value={form.id_angkatan}
                onChange={(e) =>
                  setForm({ ...form, id_angkatan: e.target.value })
                }
              >
                {angkatan.map((val) => (
                  <MenuItem key={val.id_angkatan} value={val.id_angkatan}>
                    {val.nama_angkatan}
                  </MenuItem>
                ))}
              </TextField>
            </Validate>

            <Validate
              name="password"
              required={[true, 'Harus diisi']}
              reference={passRef}
              triggers={confirmRef}
            >
              <TextField
                variant="outlined"
                label="Password"
                value={form.password}
                onChange={(e) => setForm({ ...form, password: e.target.value })}
              />
            </Validate>
            <Validate
              name="konfirmasi"
              required={[true, 'Harus diisi']}
              reference={confirmRef}
              triggers={passRef}
              custom={[
                (val) => val.toLowerCase() === form.password.toLowerCase(),
                'Konfirmasi password harus sama dengan password',
              ]}
            >
              <TextField
                variant="outlined"
                label="Konfirmasi Password"
                value={confirm}
                onChange={(e) => setConfirm(e.target.value)}
              />
            </Validate>
          </ValidationGroup>
        </Stack>
        <Stack spacing={1} mt={2}>
          <Button variant="contained" type="submit">
            Register
          </Button>
          <Button
            onClick={() => router.replace('/login')}
            variant="contained"
            sx={{
              backgroundColor: '#939393',

              '&:hover': {
                backgroundColor: '#939393',
              },
            }}
          >
            Login
          </Button>
        </Stack>
      </form>
    </Stack>
  );
}

export default Register;
