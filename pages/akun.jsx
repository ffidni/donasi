import { Button, Stack, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useContext, useEffect } from 'react';
import { AuthContext } from '../providers/AuthProvider';

export default function Akun() {
  const { user, validateUser, logout } = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    console.log(user);
    validateUser();
    if (!user) {
      router.push('/login');
    }
  }, []);

  return (
    <Stack mx="13vw">
      {user ? (
        <>
          <Typography variant="h5" textAlign="center">
            Akun
          </Typography>
          <Stack>
            <Stack my={3}>
              <Typography>Nomor HP: {user && user.nomor_hp}</Typography>
              <Typography>Nama: {user && user.nama_koordinator}</Typography>
            </Stack>
            <Button variant="contained" onClick={() => logout()}>
              Keluar
            </Button>
          </Stack>
        </>
      ) : (
        <Typography variant="h5">Anda belum login</Typography>
      )}
    </Stack>
  );
}
