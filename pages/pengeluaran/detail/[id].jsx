import { Button, MenuItem, TextField, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import { ValidationGroup, Validate } from 'mui-validate';
import { getAngkatanData } from '../../../utility/network/lib/angkatan';
import validateInputs from '../../../utility/validateInputs';
import { AuthContext } from '../../../providers/AuthProvider';
import DateRangeIcon from '@mui/icons-material/DateRange';
import {
  deletePengeluaran,
  editPengeluaran,
  getPengeluaran,
} from '../../../utility/network/lib/pengeluaran';
import { format, parseISO } from 'date-fns';
import { ErrorContext } from '../../../providers/ErrorProvider';
import convertRupiah from '../../../utility/convertRupiah';

const DetailPengeluaran = () => {
  const router = useRouter();
  const [id, setId] = useState(null);
  const { user, validateUser } = useContext(AuthContext);
  const { showAlert } = useContext(ErrorContext);

  const [form, setForm] = useState({
    jumlah_pengeluaran: '',
    keterangan: '',
    tanggal: new Date(),
  });

  function getPengeluaranData() {
    getPengeluaran(id)
      .then((res) => {
        setForm({
          jumlah_pengeluaran:
            res.data.pengeluaran.jumlah_pengeluaran.toString(),
          keterangan: res.data.pengeluaran.keterangan,
          tanggal: format(new Date(res.data.pengeluaran.tanggal), 'yyyy-MM-dd'),
        });
      })
      .catch((err) => {
        if (!err.message.includes('time'))
          showAlert('Gagal mendapatkan data. Periksa internet anda');
      });
  }

  function editPengeluaranData(elements) {
    const valid = validateInputs(elements);
    if (valid || form.jumlah_pengeluaran || form.keterangan || form.tanggal) {
      editPengeluaran(id, form)
        .then((res) => {
          console.log('Berhasil diedit!');
          showAlert('Berhasil merubah data', 'success');
        })
        .catch((err) => {
          console.log(err);
          showAlert('Gagal merubah data. Periksa internet anda');
        });
    }
  }

  function deletePengeluaranData() {
    deletePengeluaran(id)
      .then((res) => {
        showAlert('Berhasil menghapus data', 'success');

        router.back();
      })
      .catch((err) => {
        console.log(err);
        showAlert('Gagal menghapus data. Periksa internet anda');
      });
  }

  useEffect(() => {
    if (router.isReady) {
      const { id } = router.query;
      setId(id);
      validateUser();
    }
  }, [router.isReady]);

  useEffect(() => {
    if (id) {
      getPengeluaranData();
    }
  }, [id]);

  return (
    <Stack mx="13vw" key={form}>
      <Typography variant="h5" textAlign="center">
        Detail Pengeluaran
      </Typography>

      {user && user.tipe_user !== 'user' ? (
        <form onSubmit={(e) => editPengeluaranData(e)}>
          <Stack my={3} spacing={2.5}>
            <ValidationGroup initialState={{}}>
              <Validate
                name="jumlah"
                required={[true, 'Harus diisi']}
                regex={[/^[0-9\b]+$/, 'Harus angka']}
              >
                <TextField
                  label="Jumlah Pengeluaran"
                  value={form.jumlah_pengeluaran}
                  defaultValue={form.jumlah_pengeluaran}
                  onChange={(e) =>
                    setForm({ ...form, jumlah_pengeluaran: e.target.value })
                  }
                />
              </Validate>
              <Validate nama="keterangan" required={[true, 'Harus diisi']}>
                <TextField
                  label="Keterangan"
                  value={form.keterangan}
                  onChange={(e) =>
                    setForm({ ...form, keterangan: e.target.value })
                  }
                />
              </Validate>
              <Validate name="tanggal" required={[true, 'Harus diisi']}>
                <TextField
                  label="Tanggal"
                  type="date"
                  value={form.tanggal}
                  onChange={(e) => {
                    console.log(e.target.value, 'AA');
                    setForm({
                      ...form,
                      tanggal: e.target.value,
                    });
                  }}
                  InputProps={{
                    endAdornment: <DateRangeIcon fontSize="medium" />,
                  }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Validate>
            </ValidationGroup>
          </Stack>
          <Stack spacing={1} mt={2}>
            <Button variant="contained" type="submit">
              Edit
            </Button>
            <Button
              onClick={() => deletePengeluaranData()}
              variant="contained"
              sx={{
                backgroundColor: 'red',
                '&:hover': { backgorundColor: 'red' },
                '&:active': { backgorundColor: 'red' },
              }}
            >
              Hapus
            </Button>
          </Stack>
        </form>
      ) : (
        <Stack spacing={2} pt={3} justifyContent="center">
          <Stack>
            <Typography fontWeight="bold">Jumlah Pengeluaran</Typography>
            <Typography>
              {form && convertRupiah(form.jumlah_pengeluaran)}
            </Typography>
          </Stack>
          <Stack>
            <Typography fontWeight="bold">Nominal</Typography>
            <Typography>{form && form.keterangan}</Typography>
          </Stack>
          <Stack>
            <Typography fontWeight="bold">Tanggal</Typography>
            <Typography>
              {form && new Date(form.tanggal).toLocaleDateString('en-US')}
            </Typography>
          </Stack>
        </Stack>
      )}
    </Stack>
  );
};

export default DetailPengeluaran;
