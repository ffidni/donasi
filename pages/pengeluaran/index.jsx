import {
  Box,
  IconButton,
  List,
  ListItem,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import Plus from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import React, { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { AuthContext } from '../../providers/AuthProvider';
import { getPengeluaran } from '../../utility/network/lib/pengeluaran';
import { ErrorContext } from '../../providers/ErrorProvider';
import convertRupiah from '../../utility/convertRupiah';

function Pengeluaran() {
  const router = useRouter();
  const { user, validateUser } = useContext(AuthContext);
  const [searchMode, setSearchMode] = useState(false);
  const [pengeluaran, setPengeluaran] = useState([]);
  const [filterPengeluaran, setFilterPengeluaran] = useState([]);
  const [total, setTotal] = useState(0);
  const { showAlert } = useContext(ErrorContext);

  function getPengeluaranData() {
    getPengeluaran()
      .then((res) => {
        setPengeluaran(res.data.pengeluaran);
        setFilterPengeluaran(res.data.pengeluaran);
        setTotal(res.data.totalNominal);
      })
      .catch((err) =>
        showAlert('Gagal mendapatkan data. Periksa internet anda')
      );
  }

  function filter(val) {
    val = val.toLowerCase();
    setFilterPengeluaran(() => {
      let result = pengeluaran.filter(
        (item) =>
          item.jumlah_pengeluaran.toString().toLowerCase().includes(val) ||
          item.keterangan.toLowerCase().includes(val)
      );
      return result;
    });
  }

  useEffect(() => {
    validateUser();
    getPengeluaranData();
    console.log(user);
  }, []);

  return (
    <Stack>
      <Typography variant="h5" textAlign="center">
        Laporan Pengeluaran
      </Typography>
      <Stack justifyContent="center" alignItems="center" spacing={2}>
        <Box
          mt={3}
          sx={{ backgroundColor: '#D9D9D9', border: '1px solid black' }}
          py={3}
          px={5}
        >
          <Stack alignItems="center" justifyContent="center">
            <Typography variant="h4">{convertRupiah(total)}</Typography>
            <Typography mt={1} variant="h6" fontWeight="400">
              Total Pengeluaran
            </Typography>
          </Stack>
        </Box>
      </Stack>
      <Stack mt={5}>
        <Stack direction="row" alignItems="center" justifyContent="center">
          <TextField
            placeholder="Masukan kata kunci"
            label="Cari laporan"
            variant="standard"
            onChange={(e) => filter(e.target.value)}
          />

          {searchMode ? (
            <IconButton onClick={() => setSearchMode(false)}>
              <CloseIcon fontSize="medium" />
            </IconButton>
          ) : (
            <IconButton onClick={() => setSearchMode(true)}>
              <SearchIcon fontSize="medium" />
            </IconButton>
          )}
          {user && user.tipe_user === 'admin' ? (
            <IconButton onClick={() => router.push('/pengeluaran/tambah')}>
              <Plus fontSize="medium" />
            </IconButton>
          ) : null}
        </Stack>
        <Stack alignItems="center">
          <List>
            {filterPengeluaran.length === 0 ? (
              <Typography textAlign="center" variant="body1">
                Data tidak ditemukan
              </Typography>
            ) : (
              filterPengeluaran.map((item) => {
                return (
                  <ListItem
                    key={item}
                    onClick={() =>
                      router.push(`pengeluaran/detail/${item.id_pengeluaran}`)
                    }
                  >
                    <Stack
                      py={1}
                      flex={1}
                      width={'90vw'}
                      maxWidth={600}
                      justifyContent="space-between"
                      px={2}
                      boxShadow="2px 3px 3px -1px rgba(0, 0, 0, 0.3);"
                    >
                      <Typography fontWeight="bold">
                        Nominal: {convertRupiah(item.jumlah_pengeluaran)}
                      </Typography>
                      <Typography>{item.keterangan}</Typography>
                    </Stack>
                  </ListItem>
                );
              })
            )}
          </List>
        </Stack>
      </Stack>
    </Stack>
  );
}

export default Pengeluaran;
