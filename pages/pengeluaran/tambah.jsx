import { Button, MenuItem, TextField, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { ValidationGroup, Validate } from 'mui-validate';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../providers/AuthProvider';
import validateInputs from '../../utility/validateInputs';
import DateRangeIcon from '@mui/icons-material/DateRange';
import { addPengeluaran } from '../../utility/network/lib/pengeluaran';
import { ErrorContext } from '../../providers/ErrorProvider';

function Tambah() {
  const router = useRouter();
  const { user } = useContext(AuthContext);
  const { showAlert } = useContext(ErrorContext);

  const [form, setForm] = useState({
    jumlah_pengeluaran: '0',
    keterangan: '',
    tanggal: '',
  });

  function onSubmit(elements) {
    console.log(form);
    const valid = validateInputs(elements);
    if (valid) {
      addPengeluaran(form)
        .then((res) => {
          showAlert('Berhasil menambah data', 'success');

          router.back();
        })
        .catch((err) => {
          console.log(err);
          showAlert('Gagal menambah laporan. Periksa internet anda');
        });
    }
  }

  return (
    <Stack mx="13vw">
      <Typography variant="h5" textAlign="center">
        Tambah Laporan Pengeluaran
      </Typography>
      <form onSubmit={(e) => onSubmit(e)}>
        <Stack my={3} spacing={2.5}>
          <ValidationGroup>
            <Validate
              name="jumlah_pengeluaran"
              required
              regex={[/^[0-9\b]+$/, 'Harus angka']}
            >
              <TextField
                label="Jumlah Pengeluaran"
                value={form.jumlah_pengeluaran}
                onChange={(e) =>
                  setForm({ ...form, jumlah_pengeluaran: e.target.value })
                }
              />
            </Validate>
            <Validate nama="keterangan" required>
              <TextField
                label="Keterangan"
                value={form.keterangan}
                onChange={(e) =>
                  setForm({ ...form, keterangan: e.target.value })
                }
              />
            </Validate>
            <Validate name="tanggal" required={[true, 'Harus diisi']}>
              <TextField
                label="Tanggal"
                type="date"
                value={form.tanggal}
                onChange={(e) => setForm({ ...form, tanggal: e.target.value })}
                InputProps={{
                  endAdornment: <DateRangeIcon fontSize="medium" />,
                }}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Validate>
          </ValidationGroup>
        </Stack>
        <Stack spacing={1} mt={2}>
          <Button variant="contained" type="submit">
            Tambah
          </Button>
        </Stack>
      </form>
    </Stack>
  );
}

export default Tambah;
