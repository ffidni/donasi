import React, { useEffect, useState } from 'react';
import { CacheProvider } from '@emotion/react';
import {
  ThemeProvider,
  CssBaseline,
  AppBar,
  Container,
  Stack,
  Typography,
  IconButton,
  Box,
  SwipeableDrawer,
  List,
  ListItem,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import createEmotionCache from '../utility/createEmotionCache';
import '../styles/globals.css';
import theme from '../styles/theme/main';
import { AuthProvider } from '../providers/AuthProvider';
import axiosClient from '../utility/network/apiClient';
import PriceChangeIcon from '@mui/icons-material/PriceChange';
import SummarizeIcon from '@mui/icons-material/Summarize';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import PersonIcon from '@mui/icons-material/Person';
import { useRouter } from 'next/router';
import ErrorProvider from '../providers/ErrorProvider';
import { getSaldo } from '../utility/network/lib/saldo';
import convertRupiah from '../utility/convertRupiah';
import Image from 'next/image';

const clientSideEmotionCache = createEmotionCache();

const MyApp = (props) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const router = useRouter();
  const [sidebarItem, setSidebarItem] = useState([
    {
      icon: <PriceChangeIcon fontSize="medium" sx={{ color: 'white' }} />,
      key: 'donasi',
      title: 'Donasi',
      onClick: () => router.push('/donasi'),
    },
    {
      icon: <SummarizeIcon fontSize="medium" sx={{ color: 'white' }} />,
      key: 'pengeluaran',
      title: 'Pengeluaran',
      onClick: () => router.push('/pengeluaran'),
    },

    {
      icon: <PersonIcon fontSize="medium" sx={{ color: 'white' }} />,
      key: 'akun',
      title: 'Akun',
      onClick: () => router.push('/akun'),
    },
  ]);
  const [saldo, setSaldo] = useState(0);

  async function getSaldoData() {
    getSaldo()
      .then((res) => setSaldo(res.data.saldo))
      .catch((err) => console.log('gagal mendapakan data'));
  }

  useEffect(() => {
    getSaldoData();
    console.log('A');
  }, []);

  return (
    <CacheProvider value={emotionCache}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <AuthProvider>
          <ErrorProvider>
            <>
              <SwipeableDrawer
                anchor="right"
                open={sidebarOpen}
                onClose={() => setSidebarOpen(false)}
                onOpen={() => setSidebarOpen(true)}
              >
                <Stack sx={{ backgroundColor: '#3E9362', flex: 1, pt: 2 }}>
                  <IconButton onClick={() => setSidebarOpen(false)}>
                    <MenuIcon fontSize="large" sx={{ color: 'white' }} />
                  </IconButton>
                  <List>
                    <Stack spacing={2} mx={2}>
                      {sidebarItem.map((item) => (
                        <Stack
                          key={item}
                          alignItems="center"
                          direction="row"
                          spacing={1}
                          px={2}
                          py={2}
                          onClick={() => {
                            item.onClick();
                            setSidebarOpen(false);
                          }}
                          borderRadius={8}
                          bgcolor={
                            router.pathname.includes(item.key)
                              ? '#3a885b'
                              : '#46a56e'
                          }
                        >
                          {item.icon}
                          <Typography color="white" fontSize={18}>
                            {item.title}
                          </Typography>
                        </Stack>
                      ))}
                    </Stack>
                  </List>
                </Stack>
              </SwipeableDrawer>
              <AppBar color="primary">
                <Container maxWidth="xl">
                  <Stack
                    py={2}
                    direction="row"
                    alignItems="center"
                    justifyContent="space-between"
                  >
                    <Image src="/assets/logo.png" width={112} height={42} />

                    <Stack direction="row" alignItems="center">
                      <Typography variant="body1" color="white" ml={3}>
                        Saldo: {convertRupiah(saldo)}
                      </Typography>
                      <IconButton onClick={() => setSidebarOpen(true)}>
                        <MenuIcon sx={{ color: 'white' }} />
                      </IconButton>
                    </Stack>
                  </Stack>
                </Container>
              </AppBar>
              <Stack pt={10} bgcolor="white" minHeight="100vh" pt={13}>
                <Component {...pageProps} />
              </Stack>
            </>
          </ErrorProvider>
        </AuthProvider>
      </ThemeProvider>
    </CacheProvider>
  );
};

export default MyApp;
