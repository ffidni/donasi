import { Button, Stack, TextField, Typography } from '@mui/material';
import { useRouter } from 'next/router';
import { ValidationGroup, Validate } from 'mui-validate';
import React, { useContext, useEffect, useState } from 'react';
import validateInputs from '../utility/validateInputs';
import { getAccount, login } from '../utility/network/lib/auth';
import axiosClient from '../utility/network/apiClient';
import { AuthContext } from '../providers/AuthProvider';
import jwtDecode from 'jwt-decode';
import Cookies from 'js-cookie';
import { ErrorContext } from '../providers/ErrorProvider';

function Login() {
  const router = useRouter();
  const { user, validateUser } = useContext(AuthContext);
  const { showAlert } = useContext(ErrorContext);
  const [form, setForm] = useState({
    nomor_hp: '',
    password: '',
  });

  function onSubmit(elements) {
    console.log('onSUbmit()');
    const valid = validateInputs(elements);
    if (valid) {
      login(form)
        .then((res) => {
          if (res.data.token) {
            Cookies.set('token', res.data.token);
            validateUser();
            console.log('TEST');
            showAlert('Berhasil login!', 'success');

            router.replace('/donasi');
          } else {
            console.log(res);
          }
        })
        .catch((err) => {
          console.log(err, 'GAGAL');
          showAlert('Nomor HP atau password salah!');
        });
    }
  }

  useEffect(() => {
    if (user) router.push('/akun');
  }, []);

  return (
    <Stack mx="13vw">
      <Typography variant="h5" textAlign="center">
        Akun
      </Typography>
      <form onSubmit={(e) => onSubmit(e)}>
        <Stack my={3} spacing={2.5}>
          <ValidationGroup>
            <Validate name="nomor_hp" required={[true, 'Harus diisi']}>
              <TextField
                variant="outlined"
                label="Nomor HP"
                value={form.nomor_hp}
                onChange={(e) => setForm({ ...form, nomor_hp: e.target.value })}
              />
            </Validate>
            <Validate name="pass" required={[true, 'Harus diisi']}>
              <TextField
                variant="outlined"
                type="password"
                label="Password"
                value={form.password}
                onChange={(e) => setForm({ ...form, password: e.target.value })}
              />
            </Validate>
          </ValidationGroup>
        </Stack>

        <Stack spacing={1} mt={2}>
          <Button variant="contained" type="submit">
            Login
          </Button>
        </Stack>
      </form>
    </Stack>
  );
}

export default Login;
