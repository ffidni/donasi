import { Button, MenuItem, TextField, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { ValidationGroup, Validate } from 'mui-validate';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import { AuthContext } from '../../providers/AuthProvider';
import { ErrorContext } from '../../providers/ErrorProvider';
import { getAngkatanData } from '../../utility/network/lib/angkatan';
import { addDonatur } from '../../utility/network/lib/donatur';
import validateInputs from '../../utility/validateInputs';

function Tambah() {
  const router = useRouter();
  const { angkatan, setAngkatan, user } = useContext(AuthContext);
  const [form, setForm] = useState({ nama: '', id_angkatan: '', nominal: '0' });
  const { showAlert } = useContext(ErrorContext);

  function onSubmit(elements) {
    console.log(form);
    const valid = validateInputs(elements);
    if (valid) {
      addDonatur(form)
        .then((res) => {
          showAlert('Berhasil menambah data', 'success');

          router.replace('/donasi');
        })
        .catch((err) => {
          console.log(err);
          showAlert('Gagal menambah donatur. Periksa internet anda');
        });
    }
  }

  useEffect(() => {
    getAngkatanData(setAngkatan);
  }, []);

  useEffect(() => {
    console.log(angkatan);
  }, [angkatan]);

  return (
    <Stack mx="13vw">
      <Typography variant="h5" textAlign="center">
        Tambah Donatur
      </Typography>
      <form onSubmit={(e) => onSubmit(e)}>
        <Stack my={3} spacing={2.5}>
          <ValidationGroup>
            <Validate name="nama" required>
              <TextField
                label="Nama Donatur"
                value={form.nama}
                onChange={(e) => setForm({ ...form, nama: e.target.value })}
              />
            </Validate>
            <Validate
              nama="nominal"
              required
              regex={[/^[0-9\b]+$/, 'Harus angka']}
            >
              <TextField
                label="Nominal"
                value={form.nominal}
                onChange={(e) => setForm({ ...form, nominal: e.target.value })}
              />
            </Validate>
            <Validate name="angkatan" required={[true, 'Harus diisi']}>
              <TextField
                variant="outlined"
                label="Angkatan"
                select
                value={form.id_angkatan}
                onChange={(e) =>
                  setForm({ ...form, id_angkatan: e.target.value })
                }
              >
                {angkatan.map((val) => (
                  <MenuItem key={val.id_angkatan} value={val.id_angkatan}>
                    {val.nama_angkatan}
                  </MenuItem>
                ))}
              </TextField>
            </Validate>
          </ValidationGroup>
        </Stack>
        <Stack spacing={1} mt={2}>
          <Button variant="contained" type="submit">
            Tambah
          </Button>
        </Stack>
      </form>
    </Stack>
  );
}

export default Tambah;
