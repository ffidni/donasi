import { Button, MenuItem, TextField, Typography } from '@mui/material';
import { Stack } from '@mui/system';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import {
  deleteDonatur,
  editDonatur,
  getDonatur,
} from '../../../utility/network/lib/donatur';
import { ValidationGroup, Validate } from 'mui-validate';
import { getAngkatanData } from '../../../utility/network/lib/angkatan';
import validateInputs from '../../../utility/validateInputs';
import { AuthContext } from '../../../providers/AuthProvider';
import DateRangeIcon from '@mui/icons-material/DateRange';
import { format } from 'date-fns';
import { ErrorContext } from '../../../providers/ErrorProvider';
import convertRupiah from '../../../utility/convertRupiah';

const DetailDonatur = () => {
  const router = useRouter();
  const [id, setId] = useState(null);
  const { angkatan, setAngkatan, user, validateUser } = useContext(AuthContext);
  const { showAlert } = useContext(ErrorContext);

  const [form, setForm] = useState({
    nama: '',
    nominal: '',
    tanggal: '',
    id_angkatan: 1,
  });

  function getDonaturData() {
    getDonatur(id)
      .then((res) => {
        console.log(res.data.donatur);
        setForm({
          nama: `${res.data.donatur.nama}`,
          id_angkatan: res.data.donatur.id_angkatan,
          nominal: `${res.data.donatur.nominal}`,
          tanggal: format(new Date(res.data.donatur.tanggal), 'yyyy-MM-dd'),
        });
      })
      .catch((err) => {
        if (!err.message.includes('time'))
          showAlert('Gagal mendapatkan data. Periksa internet anda');
      });
  }

  function deleteDonaturData() {
    deleteDonatur(id)
      .then((res) => {
        showAlert('Berhasil menghapus data', 'success');

        router.back();
      })
      .catch((err) => showAlert('Gagal menghapus data. Periksa internet anda'));
  }

  function editDonaturData(elements) {
    const valid = validateInputs(elements);
    if (
      valid ||
      (form.nama && form.nominal && form.tanggal && form.id_angkatan)
    ) {
      editDonatur(id, form)
        .then((res) => {
          console.log('BERHASIL');
          showAlert('Berhasil merubah data', 'success');
        })
        .catch((err) => showAlert('Gagal merubah data. Periksa internet anda'));
    }
  }

  useEffect(() => {
    if (router.isReady) {
      const { id } = router.query;
      setId(id);
      validateUser();
    }
  }, [router.isReady]);

  useEffect(() => {
    console.log(id);
    getDonaturData();
    getAngkatanData(setAngkatan);
  }, [id]);

  return (
    <Stack mx="13vw" key={form}>
      <Typography variant="h5" textAlign="center">
        Detail Donasi
      </Typography>

      {user && user.tipe_user !== 'user' ? (
        <form onSubmit={(e) => editDonaturData(e)}>
          <Stack my={3} spacing={2.5}>
            <ValidationGroup initialState={form}>
              <Validate name="nama" required={[true, 'Harus diisi']}>
                <TextField
                  label="Nama"
                  value={form.nama}
                  onChange={(e) => setForm({ ...form, nama: e.target.value })}
                />
              </Validate>
              <Validate
                nama="nominal"
                required={[true, 'Harus diisi']}
                regex={[/^[0-9\b]+$/, 'Harus angka']}
              >
                <TextField
                  label="Nominal"
                  value={form.nominal}
                  onChange={(e) =>
                    setForm({ ...form, nominal: e.target.value })
                  }
                />
              </Validate>
              <Validate name="angkatan" required={[true, 'Harus diisi']}>
                <TextField
                  variant="outlined"
                  label="Angkatan"
                  select
                  defaultValue={form.id_angkatan}
                  value={form.id_angkatan}
                  onChange={(e) =>
                    setForm({ ...form, id_angkatan: e.target.value })
                  }
                >
                  {angkatan.map((val) => (
                    <MenuItem key={val.id_angkatan} value={val.id_angkatan}>
                      {val.nama_angkatan}
                    </MenuItem>
                  ))}
                </TextField>
              </Validate>
              <Validate name="tanggal" required={[true, 'Harus diisi']}>
                <TextField
                  label="Tanggal"
                  type="date"
                  value={form.tanggal}
                  onChange={(e) => {
                    setForm({
                      ...form,
                      tanggal: e.target.value,
                    });
                  }}
                  InputProps={{
                    endAdornment: <DateRangeIcon fontSize="medium" />,
                  }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </Validate>
            </ValidationGroup>
          </Stack>
          <Stack spacing={1} mt={2}>
            <Button variant="contained" type="submit">
              Edit
            </Button>
            <Button
              onClick={() => deleteDonaturData()}
              variant="contained"
              sx={{
                backgroundColor: 'red',
                '&:hover': { backgorundColor: 'red' },
                '&:active': { backgorundColor: 'red' },
              }}
            >
              Hapus
            </Button>
          </Stack>
        </form>
      ) : (
        <Stack spacing={2} pt={3} justifyContent="center">
          <Stack>
            <Typography fontWeight="bold">Nama Donatur</Typography>
            <Typography>{form && form.nama}</Typography>
          </Stack>
          <Stack>
            <Typography fontWeight="bold">Nominal</Typography>
            <Typography>{form && convertRupiah(form.nominal)}</Typography>
          </Stack>
          <Stack>
            <Typography fontWeight="bold">Angkatan</Typography>
            <Typography>{form && form.nama}</Typography>
          </Stack>
          <Stack>
            <Typography fontWeight="bold">Tanggal</Typography>
            <Typography>
              {form && new Date(form.tanggal).toLocaleDateString('en-US')}
            </Typography>
          </Stack>
        </Stack>
      )}
    </Stack>
  );
};

export default DetailDonatur;
