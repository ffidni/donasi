import {
  Box,
  IconButton,
  List,
  ListItem,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import Plus from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import React, { useContext, useEffect, useState } from 'react';
import { getDonatur } from '../../utility/network/lib/donatur';
import { AuthContext } from '../../providers/AuthProvider';
import { useRouter } from 'next/router';
import { ErrorContext } from '../../providers/ErrorProvider';
import convertRupiah from '../../utility/convertRupiah';

const Donasi = () => {
  const router = useRouter();
  const [dataDonasi, setDataDonasi] = useState({});
  const [donatur, setDonatur] = useState([]);
  const [searchMode, setSearchMode] = useState(false);
  const { user, validateUser } = useContext(AuthContext);
  const { showAlert } = useContext(ErrorContext);

  function getDonaturData() {
    getDonatur()
      .then((res) => {
        setDataDonasi(res.data);
        setDonatur(res.data.donatur);
      })
      .catch((err) => {
        console.log(err);
        showAlert('Gagal mendapatkan data. Periksa internet anda');
      });
  }

  function filter(val) {
    val = val.toLowerCase();
    setDonatur(() => {
      let result = dataDonasi.donatur.filter(
        (item) =>
          item.nama.toLowerCase().includes(val) ||
          item.angkatan.includes(val) ||
          item.nominal.toString().includes(val)
      );
      console.log(result, val, dataDonasi.donatur);
      return result;
    });
  }

  useEffect(() => {
    console.log(user);
  }, [user]);

  useEffect(() => {
    if (!searchMode && dataDonasi.donatur) setDonatur(dataDonasi.donatur);
  }, [searchMode]);

  useEffect(() => {
    console.log(user);
    validateUser();
    getDonaturData();
  }, []);

  return (
    <Stack>
      <Typography variant="h5" textAlign="center">
        Jumlah Donasi Masuk
      </Typography>
      <Stack justifyContent="center" alignItems="center" spacing={2}>
        <Box
          mt={3}
          sx={{ backgroundColor: '#D9D9D9', border: '1px solid black' }}
          py={3}
          px={5}
        >
          <Stack alignItems="center" justifyContent="center">
            <Typography variant="h4">
              {convertRupiah(
                !dataDonasi.totalNominal ? '0' : dataDonasi.totalNominal
              )}
            </Typography>
            <Typography mt={1} variant="h6" fontWeight="400">
              Terkumpul
            </Typography>
          </Stack>
        </Box>
        <Typography>Rek. donasi: BRI 4401-2020-1325-25</Typography>
      </Stack>
      <Stack mt={5}>
        <Stack direction="row" alignItems="center" justifyContent="center">
          {searchMode ? (
            <TextField
              placeholder="Masukan nama donatur"
              label="Cari Donatur"
              variant="standard"
              onChange={(e) => filter(e.target.value)}
            />
          ) : (
            <Typography
              textAlign="center"
              mt={1}
              variant="h6"
              fontWeight="bold"
            >
              Daftar Donatur
            </Typography>
          )}

          {searchMode ? (
            <IconButton onClick={() => setSearchMode(false)}>
              <CloseIcon fontSize="medium" />
            </IconButton>
          ) : (
            <IconButton onClick={() => setSearchMode(true)}>
              <SearchIcon fontSize="medium" />
            </IconButton>
          )}
          {user && user.tipe_user === 'admin' ? (
            <IconButton onClick={() => router.push('/donasi/tambah')}>
              <Plus fontSize="medium" />
            </IconButton>
          ) : null}
        </Stack>
        <Stack alignItems="center">
          <List>
            {donatur.length === 0 ? (
              <Typography textAlign="center" variant="body1">
                Data tidak ditemukan
              </Typography>
            ) : (
              donatur.map((donatur) => {
                return (
                  <ListItem
                    key={donatur}
                    onClick={() =>
                      router.push(`/donasi/detail/${donatur.id_donatur}`)
                    }
                  >
                    <Stack
                      py={1}
                      direction="row"
                      flex={1}
                      width={'90vw'}
                      maxWidth={600}
                      justifyContent="space-between"
                      px={2}
                      boxShadow="2px 3px 3px -1px rgba(0, 0, 0, 0.3);"
                      alignItems="center"
                    >
                      <Typography fontWeight="bold">{donatur.nama}</Typography>
                      <Typography>Angkatan: {donatur.angkatan}</Typography>
                      <Typography fontWeight="bold">
                        {convertRupiah(donatur.nominal)}
                      </Typography>
                    </Stack>
                  </ListItem>
                );
              })
            )}
          </List>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default Donasi;
